TESTS=5
SEEDS=(0 1 2 4 11 42 5034)
POWER=(1 10)
COL_C=(1 1 1 2 2 2 5)
COL_P=(1 2 3 2 4 8 10)
#Rozmiar - 1 tj. ostatni element
COL_SIZE=6

INNER_C=(1 1 1 2 2 3)
INNER_P=(1 2 3 4 8 9)
#Rozmiar - 1 tj. ostatni element
INNER_SIZE=5

GCOUNTS=(-1000 -150 -50 0 50 150 1000)

make

for SEED in "${SEEDS[@]}"; do
    for REPS in "${POWER[@]}"; do
        ./generate $SEED $REPS 0 > input

        for IDX in $(seq 0 $COL_SIZE); do
            C=${COL_C[$IDX]};
            P=${COL_P[$IDX]};
            START=$(date +%s.%N)
            mpirun -np $P ../matrixmul -f input -s $SEED -c $C -v -e $REPS > temp_res
            END=$(date +%s.%N)
            DIFF=$(echo "$END - $START" | bc)
            ./compare temp_res res
            if [ $? -eq 0  ]
            then
                echo "TEST $SEED $REPS WITH $P $C PASSED, time: $DIFF s"
            else
                echo -e "\e[31m"
                echo "TEST $SEED $REPS WITH $P $C NOT PASSED"
                echo "RUN FOLLOWING COMMANDS:"
                echo "./generate $SEED $REPS 0 > input"
                echo "mpirun -np $P ../matrixmul -f input -s $SEED -c $C -v -e $REPS > temp_res"
                echo "./compare temp_res res"
                echo -e "\e[39m"
                exit
            fi
        done

        for IDX in $(seq 0 $INNER_SIZE); do
            C=${INNER_C[$IDX]};
            P=${INNER_P[$IDX]};
            START=$(date +%s.%N)
            mpirun -np $P ../matrixmul -f input -s $SEED -c $C -i -v -e $REPS > temp_res
            END=$(date +%s.%N)
            DIFF=$(echo "$END - $START" | bc)
            ./compare temp_res res
            if [ $? -eq 0  ]
            then
                echo "TEST $SEED $REPS WITH $P $C INNER PASSED, time: $DIFF s"
            else
                echo -e "\e[31m"
                echo "TEST $SEED $REPS  INNER NOT PASSED"
                echo "RUN FOLLOWING COMMANDS:"
                echo "./generate $SEED $REPS 0 > input"
                echo "mpirun -np $P ../matrixmul -f input -s $SEED -c $C -i -v -e $REPS > temp_res"
                echo "./compare temp_res res"
                echo -e "\e[39m"
                exit
            fi
        done
        for GCOUNT in "${GCOUNTS[@]}"; do
            ./generate $SEED $REPS 1 $GCOUNT > input
            for IDX in $(seq 0 $COL_SIZE); do
                C=${COL_C[$IDX]};
                P=${COL_P[$IDX]};
                START=$(date +%s.%N)
                mpirun -np $P ../matrixmul -f input -s $SEED -c $C -e $REPS -g $GCOUNT > temp_res
                END=$(date +%s.%N)
                DIFF=$(echo "$END - $START" | bc)
                diff temp_res res
                if [ $? -eq 0  ]
                then
                    echo "TEST $SEED $REPS GC=$GCOUNT WITH $P $C PASSED, time: $DIFF s"
                else
                    echo -e "\e[31m"
                    echo "TEST $SEED $REPS GC=$GCOUNT WITH $P $C NOT PASSED"
                    echo "RUN FOLLOWING COMMANDS:"
                    echo "./generate $SEED $REPS 1 $GCOUNT > input"
                    echo "mpirun -np $P ../matrixmul -f input -s $SEED -c $C -e $REPS -g $GCOUNT > temp_res"
                    echo "./compare temp_res res"
                    echo -e "\e[39m"
                    exit
                fi
            done

            for IDX in $(seq 0 $INNER_SIZE); do
                C=${INNER_C[$IDX]};
                P=${INNER_P[$IDX]};
                START=$(date +%s.%N)
                mpirun -np $P ../matrixmul -f input -s $SEED -c $C -i -e $REPS -g $GCOUNT > temp_res
                END=$(date +%s.%N)
                DIFF=$(echo "$END - $START" | bc)
                diff temp_res res
                if [ $? -eq 0  ]
                then
                    echo "TEST $SEED $REPS GC=$GCOUNT WITH $P $C PASSED, time: $DIFF s"
                else
                    echo -e "\e[31m"
                    echo "TEST $SEED $REPS GC=$GCOUNT WITH $P $C NOT PASSED"
                    echo "RUN FOLLOWING COMMANDS:"
                    echo "./generate $SEED $REPS 1 $GCOUNT > input"
                    echo "mpirun -np $P ../matrixmul -f input -s $SEED -c $C -e $REPS -i -g $GCOUNT > temp_res"
                    echo "./compare temp_res res"
                    echo -e "\e[39m"
                    exit
                fi
            done
        done
    done;
done;
rm -f res temp_res input
