#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "densematgen.h"

double mat[310][310];
double mat2[310][310];
double res[310][310];

void generate(int n, int el) {
    printf("%d %d %d %d\n", n, n, el, n);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            mat[i][j] = 0.0f;
        }
    }

    for (int i = 0; i < el; ++i) {
        int x, y;
        x = rand()%n;
        y = rand()%n;
        int z = rand()%10000;
        if (mat[x][y] == 0.0f) {
            mat[x][y] = (double)(z) / 10000;
        } else {
            i--;
            continue;
        }
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (mat[i][j] != 0.0f) {
                printf("%lf ", mat[i][j]);
            }
       }
    }
    printf("\n");

    printf("0 ");
    int sum = 0;
    for (int i = 0; i < n; ++i) {
        int cnt = 0;
        for (int j = 0; j < n; ++j) {
            if (mat[i][j] != 0.0f) {
                cnt++;
            }
       }
        sum += cnt;
        printf("%d ", sum);
    }
    printf("\n");

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (mat[i][j] != 0.0f) {
                printf("%d ", j);
            }
       }
    }
    printf("\n");
}

void gen_two(int n, int seed) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            mat2[i][j] = generate_double(seed, i, j);
        }
    }
}

void mul_res(int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            res[i][j] = 0.0f;
        }
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                res[i][j] += mat[i][k] * mat2[k][j];
            }
        }
    }
}

void print_res(int n, int g_count, double count_val) {
    FILE *fp;

    if ((fp = fopen("res", "w+")) == NULL) {
        fprintf(stderr, "Couldnt open: res file\n");
    }


    if (!g_count) {
        fprintf(fp, "%d %d\n", n, n);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                fprintf(fp, "%lf ", res[i][j]);
            }
            fprintf(fp, "\n");
        }
    }
    else {
        int counter = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                counter += res[i][j] >= count_val;
            }
        }
        fprintf(fp, "%d\n", counter);
    }
}

int main(int argc, char **argv) {
    int seed = atoi(argv[1]);
    int reps = atoi(argv[2]);
    int g_count = atoi(argv[3]);
    double count_val = 0;
    if (g_count)
        count_val = atof(argv[4]);
    srand(seed);

    int n_size = rand()%300 + 10;

    int elements = rand()%(n_size * (int)sqrt(n_size)) + n_size * sqrt(n_size);
    generate(n_size, elements);
    gen_two(n_size, seed);
    for (int i = 0; i < reps; ++i) {
        mul_res(n_size);
        memcpy(mat2, res, sizeof(double) * 310 * 310);
    }
    print_res(n_size, g_count, count_val);

    return 0;
}
