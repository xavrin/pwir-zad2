#include "alg.h"

#define PRINTF(buf, count, ...) (count += sprintf(buf + count, __VA_ARGS__))

char* print_your_part(params_t *par, int* buf_count);

char* print_your_Apart(params_t *par, int* buf_count);

void sequential_printing(params_t *par,
                         char* (*single_print)(params_t *, int*));

void mpi_breakpoint();
