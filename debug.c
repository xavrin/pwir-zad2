#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

#include "debug.h"

char* print_your_part(params_t *par, int* buf_count) {
    int i, j;
    char *buf = malloc(10000);
    *buf_count = 0;
    *buf_count += sprintf(buf + *buf_count, "rank=%d\n", par->rank);
    for (i = 0; i < par->n; i++) {
        for (j = 0; j < par->BC_col_width; j++)
            *buf_count += sprintf(buf + *buf_count, "%lf, ", GET_C(par, i, j));
        *buf_count += sprintf(buf + *buf_count, "\n");
    }
    *buf_count += sprintf(buf + *buf_count, "\n\n");
    return buf;
}

char* print_your_Apart(params_t *par, int* buf_count) {
    int i;
    char *buf = malloc(10000);
    *buf_count = 0;
    PRINTF(buf, *buf_count, "rank=%d\n", par->rank);
    for (i = 0; i < par->A_part_count; i++) {
        PRINTF(buf, *buf_count, "(elem=%lf, row=%d, col=%d)\n", par->A_part[i].elem, par->A_part[i].row, par->A_part[i].col);
    }
    PRINTF(buf, *buf_count, "\n\n");
    return buf;
}

void sequential_printing(params_t *par, char* (*single_print)(params_t *, int*)) {
    int i;
    char *send_buf;
    int send_count;
    int *recvcounts = NULL;
    int *displs = NULL;
    char *big_buffer = NULL;
    int big_count = 0;

    send_buf = single_print(par, &send_count);

    if (par->rank == 0) {
        recvcounts = malloc(par->p * sizeof(*recvcounts));
        displs = malloc(par->p * sizeof(*displs));
    }
    MPI_Gather(&send_count, 1, MPI_INT, recvcounts, 1, MPI_INT, 0,
               MPI_COMM_WORLD);
    if (par->rank == 0) {
        big_count = 0;
        for (i = 0; i < par->p; i++) {
            displs[i] = big_count;
            big_count += recvcounts[i];
        }
        big_buffer = malloc(big_count * sizeof(*big_buffer));
    }

    MPI_Gatherv(send_buf, send_count, MPI_CHAR, big_buffer, recvcounts, displs,
                MPI_CHAR, 0, MPI_COMM_WORLD);
    if (par->rank == 0) {
        for (i = 0; i < big_count; i++)
            putc(big_buffer[i], stderr);
        free(recvcounts);
        free(displs);
        free(big_buffer);
    }
    free(send_buf);
    MPI_Barrier(MPI_COMM_WORLD);
}

void mpi_breakpoint() {
    MPI_Barrier(MPI_COMM_WORLD);
    exit(42);
}
