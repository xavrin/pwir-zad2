#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>
#include <getopt.h>

#include "densematgen.h"
#include "alg.h"

void mpi_exit() {
    MPI_Finalize();
    exit(3);
}

int main(int argc, char * argv[]) {
    int show_results = 0;
    int seed = -1;
    int exponent = 1;
    int option = -1;
    int count_ge = 0;
    double comm_start = 0, comm_end = 0, comp_start = 0, comp_end = 0;
    double ge_element = 0;
    sparse_t *sparse = NULL;
    params_t par;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &par.p);
    MPI_Comm_rank(MPI_COMM_WORLD, &par.rank);
    par.c = 1;
    par.use_inner = 0;

    while ((option = getopt(argc, argv, "vis:f:c:e:g:")) != -1) {
        switch (option) {
            case 'v': show_results = 1; break;
            case 'i': par.use_inner = 1; break;
            case 'f': if (par.rank == 0) {
                if (!(sparse = read_sparse(optarg))) {
                    fprintf(stderr, "Error reading sparse file");
                    mpi_exit();
                }
            } break;
            case 'c': par.c = atoi(optarg); break;
            case 's': seed = atoi(optarg); break;
            case 'e': exponent = atoi(optarg); break;
            case 'g': count_ge = 1; ge_element = atof(optarg); break;
            default: {
                fprintf(stderr, "error parsing argument %c exiting\n", option);
                mpi_exit();
            }
        }
    }
    if ((seed == -1) || ((par.rank == 0) && (sparse == NULL))) {
        fprintf(stderr, "error: missing seed or sparse matrix file; exiting\n");
        mpi_exit();
    }
    if (par.p % par.c) {
        fprintf(stderr, "C should divide P");
        mpi_exit();
    }
    if (par.p % (par.c * par.c) && par.use_inner) {
        fprintf(stderr, "C^2 should divide P for the inner algorithm");
        mpi_exit();
    }

    MPI_Barrier(MPI_COMM_WORLD);
    comm_start =  MPI_Wtime();

    init(&par, sparse);
    init_A(&par, sparse);
    init_B(&par, seed);

    if (par.rank == 0)
        free_sparse(sparse);

    MPI_Barrier(MPI_COMM_WORLD);
    comm_end = MPI_Wtime();

    comp_start = MPI_Wtime();

    calculate_C(&par, exponent);

    MPI_Barrier(MPI_COMM_WORLD);
    comp_end = MPI_Wtime();

    if (show_results)
        print_result(&par);
    if (count_ge) {
        int count = count_elts_ge(&par, ge_element);
        if (par.rank == 0)
            printf("%d\n", count);
    }
    if (par.rank == 0) {
        double comp_time = comp_end - comp_start;
        double comm_time = comm_end - comm_start;
        fprintf(stderr, "Computation=%lf, communication=%lf, total=%lf\n",
                comp_time, comm_time, comp_time + comm_time);
    }
    cleanup(&par);
    MPI_Finalize();
    return 0;
}
