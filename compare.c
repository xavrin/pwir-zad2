#include <stdio.h>
#include <math.h>

#define EPS 0.001

int main(int argc, char **argv) {
    FILE *file = fopen(argv[1], "r");
    FILE *file2 = fopen(argv[2], "r");

    int n1, m1;
    int n2, m2;

    fscanf(file, "%d %d", &n1, &m1);
    fscanf(file2, "%d %d", &n2, &m2);

    if (n1 != n2 || m1 != m2) {
        printf("Different matrix sizes\n");
        return 1;
    }

    int i = 0;
    while(i < n1 * m1) {
        double x1, y1;
        fscanf(file, "%lf", &x1);
        fscanf(file2, "%lf", &y1);
        if (fabs(x1 - y1) > EPS) {
            printf("Different %d-th value %lf vs %lf \n", i, x1, y1);
            return 1;
        }
        ++i;
    }


    return 0;
}
