#ifndef __PWIR__ALL__H__
#define __PWIR__ALL__H__

#include <mpi.h>

#define GET_B(par, _row, _col) (par->B_part)[(_row) * (par->BC_col_width) + (_col)]
#define GET_C(par, _row, _col) (par->C_part)[(_row) * (par->BC_col_width) + (_col)]

typedef struct {
    double elem;
    int row;
    int col;
} sparse_elem_t;

/* All parameters needed for a single computation. */
typedef struct {
    int n_orig; // original size of the matrix, needed for getting the correct
                // result
    int n; // size of the matrix after extending size to the multiplicity of p
    int p; // number of processes
    int c; // size of the replication group
    int pdivc;
    int q; // p / c^2
    int ndivp;
    int nonz; // number of nonzero elements in A
    int rank; // rank of the current process
    int use_inner;

    sparse_elem_t *A_part; // part of A for the given process
    sparse_elem_t *A_part_tmp; // buffer needed for shift operations
    int A_part_count; // number of elements in the part of A for this process

    double *B_part; // part of B for the given process

    double *C_part; // part of C for the given process
    int BC_col_width; // number of columns of C_part per process
    int BC_part_count; // count of each of the buffers above,
                       // (but not the size of it which is actually bigger
                       // for the inner algorithm to make life easier)
    MPI_Comm shift_comm; // communicator in which processes with subsequent
                         // ranks have subsequent parts of matrix A
    // the values below directly correspond with the description of the inner
    // algorithm
    int l; // number of the process inside the replication group
    int j; // number of the replication group of the process
} params_t;

typedef struct {
    int rows_num;
    int cols_num;
    int nonz_num;
    int max_nonz_row_num;
    double *entries; // of size nonz_num
    int *offsets; // of size rows_num + 1
    int *col_idxs; // of size nonz_num
} sparse_t;

MPI_Datatype mpi_sparse_elem_t;

sparse_t* read_sparse(const char *filename);
void free_sparse(sparse_t *sparse);

void init(params_t *par, sparse_t *sparse);
void cleanup(params_t *par);

void init_A(params_t *par, sparse_t *sparse);
void init_B(params_t *par, int seed);

void calculate_C(params_t *par, int exponent);

int count_elts_ge(params_t *par, double threshold);
void print_result(params_t *par);

#endif // __PWIR__ALL__H__
