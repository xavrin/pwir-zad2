#include <mpi.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "alg.h"
#include "densematgen.h"

#define MULTIPLY_TAG 91345

#define SWAP(T, x, y) do { \
    T tmp = x;             \
    x = y;                 \
    y = tmp;               \
} while (0)

sparse_t* read_sparse(const char *filename) {
    int i;
    FILE *f;
    sparse_t *sparse;

    if (!(sparse = malloc(sizeof(*sparse))))
        return NULL;
    if (!(f = fopen(filename, "r")))
        return NULL;
    if (4 != fscanf(f, "%d %d %d %d", &sparse->rows_num,
                                      &sparse->cols_num,
                                      &sparse->nonz_num,
                                      &sparse->max_nonz_row_num))
        return NULL;

    if (!(sparse->entries = malloc(sparse->nonz_num *
                                   sizeof(*sparse->entries))))
        return NULL;
    for (i = 0; i < sparse->nonz_num; ++i)
        if (1 != fscanf(f, "%lf", &sparse->entries[i]))
            return NULL;

    if (!(sparse->offsets = malloc((sparse->rows_num + 1) *
                                   sizeof(*sparse->offsets))))
        return NULL;
    for (i = 0; i < sparse->rows_num + 1; ++i)
        if (1 != fscanf(f, "%d", &sparse->offsets[i]))
            return NULL;

    if (!(sparse->col_idxs = malloc(sparse->nonz_num *
                                    sizeof(*sparse->col_idxs))))
        return NULL;
    for (i = 0; i < sparse->nonz_num; ++i)
        if (1 != fscanf(f, "%d", &sparse->col_idxs[i]))
            return NULL;

    return sparse;
}

void free_sparse(sparse_t *sparse) {
    free(sparse->col_idxs);
    free(sparse->offsets);
    free(sparse->entries);
    free(sparse);
}

static void init_types() {
    MPI_Datatype types[3] = {MPI_DOUBLE, MPI_INT, MPI_INT};
    MPI_Aint offsets[3];
    int blocklengths[3] = {1, 1, 1};

    offsets[0] = offsetof(sparse_elem_t, elem);
    offsets[1] = offsetof(sparse_elem_t, row);
    offsets[2] = offsetof(sparse_elem_t, col);
    MPI_Type_create_struct(3, blocklengths, offsets, types, &mpi_sparse_elem_t);
    MPI_Type_commit(&mpi_sparse_elem_t);
}

static void clean_types() {
    MPI_Type_free(&mpi_sparse_elem_t);
}

void init(params_t *par, sparse_t *sparse) {
    int BC_capacity;
    int additional_capacity;

    if (par->rank == 0) {
        par->n_orig = sparse->rows_num;
        par->n = (par->n_orig + par->p - 1) / par->p * par->p;
        par->nonz = sparse->nonz_num;
    }

    MPI_Bcast(&par->n_orig, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&par->n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&par->nonz, 1, MPI_INT, 0, MPI_COMM_WORLD);

    par->pdivc = par->p / par->c;
    par->q = par->p / (par->c * par->c);
    par->ndivp = par->n / par->p;
    if (par->use_inner)
        par->BC_col_width = par->c * par->ndivp;
    else
        par->BC_col_width = par->ndivp;
    par->BC_part_count = par->BC_col_width * par->n;
    par->l = par->rank / par->pdivc;
    par->j = par->rank % par->pdivc;

    // Additional capacity will be needed for easier merging of the results
    // in the inner algorithm.
    if (par->use_inner)
        additional_capacity = par->n / par->c;
    else
        additional_capacity = 0;

    BC_capacity = (par->n + additional_capacity) * par->BC_col_width;

    par->A_part = malloc(par->nonz * sizeof(*par->A_part));
    par->A_part_tmp = malloc(par->nonz * sizeof(*par->A_part));
    par->B_part = malloc(BC_capacity * sizeof(*par->B_part));
    par->C_part = malloc(BC_capacity * sizeof(*par->C_part));

    MPI_Comm_split(MPI_COMM_WORLD, par->l, par->j, &par->shift_comm);
    init_types();
}

void cleanup(params_t *par) {
    free(par->A_part);
    free(par->A_part_tmp);

    free(par->B_part);
    free(par->C_part);
    MPI_Comm_free(&par->shift_comm);
    clean_types();
}

void init_B(params_t *par, int seed) {
    int offset;
    int r, c;
    offset = ((par->use_inner) ? par->j : par->rank) * par->BC_col_width;

    for (r = 0; r < par->n; ++r) {
        for (c = 0; c < par->BC_col_width; ++c) {
            int absc = c + offset;
            int maxrc = (r > absc) ? r : absc;
            if (maxrc >= par->n_orig)
                GET_B(par, r, c) = 0;
            else
                GET_B(par, r, c) = generate_double(seed, r, absc);
        }
    }
}

static int int_pair_compar(int x1, int y1, int x2, int y2) {
    if (x1 != x2) {
        if (x1 < x2) return -1; else return 1;
    }
    else {
        if (y1 != y2) {
            if (y1 < y2) return -1; else return 1;
        }
        return 0;
    }
}

static int col_compar(const void *vl, const void *vr) {
    const sparse_elem_t *l = vl, *r = vr;
    return int_pair_compar(l->col, l->row, r->col, r->row);
}

static int row_compar(const void *vl, const void *vr) {
    const sparse_elem_t *l = vl, *r = vr;
    return int_pair_compar(l->row, l->col, r->row, r->col);
}

static int get_row(sparse_elem_t elem) { return elem.row; }
static int get_col(sparse_elem_t elem) { return elem.col; }

/* This function is a dispatch point for the two algorithms. It was made to
 * let the function init_A be more general. The differences will be hidden
 * in two function pointers (it concerns the fact that A will be divided either
 * by rows or by columns) and in communicators (their purpose is to take care
 * of all of messaging in a clean way).
 * repl_comm - communicator containing processes that in the further parts of
 * algorithm have the same parts of A.
 * distr_comm - global communicator, we'll use a custom communicator because
 * it will let us easily send messages in such a way that processes inside a
 * single repl_comm group will be able to build their own part of A from
 * smaller pieces.
 */
static void dispatch_init_A(params_t *par, int (**f)(const void*, const void*),
                            MPI_Comm *repl_comm, MPI_Comm *distr_comm,
                            int (**getkey)(const sparse_elem_t)) {
    if (par->use_inner) {
        int which_A_part;

        which_A_part = (par->l * par->q + par->j) % par->pdivc;

        MPI_Comm_split(MPI_COMM_WORLD, which_A_part, par->l, repl_comm);
        MPI_Comm_split(MPI_COMM_WORLD, 0, which_A_part * par->c + par->l,
                       distr_comm);
        *f = row_compar;
        *getkey = get_row;
    }
    else {
        MPI_Comm_split(MPI_COMM_WORLD, par->j, par->l, repl_comm);
        MPI_Comm_split(MPI_COMM_WORLD, 0, par->j * par->c + par->l,
                       distr_comm);
        *f = col_compar;
        *getkey = get_col;
    }
}

void init_A(params_t *par, sparse_t *sparse) {
    int *sendcounts = NULL, *displs = NULL, *recvcounts;
    MPI_Comm repl_comm, distr_comm;
    sparse_elem_t *tmp_elements;
    int tmp_count;
    int next_displ;
    int i;
    int (*compar)(const void*, const void*);
    int (*getkey)(sparse_elem_t);

    dispatch_init_A(par, &compar, &repl_comm, &distr_comm, &getkey);
    tmp_elements = malloc(par->nonz * sizeof(*tmp_elements));

    if (par->rank == 0) {
        int i, idx;
        int cur_proc;

        idx = 0;

        // Changing CSR to a list of sparse_elem_t.
        for (i = 0; i < par->n_orig; ++i) {
            while (idx < sparse->offsets[i + 1]) {
                par->A_part[idx] = (sparse_elem_t){
                    .elem = sparse->entries[idx],
                    .row  = i,
                    .col  = sparse->col_idxs[idx]};
                idx++;
            }
        }
        qsort(par->A_part, sparse->nonz_num, sizeof(*par->A_part), compar);

        sendcounts = malloc(par->p * sizeof(*sendcounts));
        displs = malloc(par->p * sizeof(*displs));

        // Dividing elements to column buckets.
        idx = 0;
        for (cur_proc = 0; cur_proc < par->p; ++cur_proc) {
            sendcounts[cur_proc] = 0;
            displs[cur_proc] = idx;
            while (getkey(par->A_part[idx]) / par->ndivp == cur_proc &&
                   idx < par->nonz) {
                ++sendcounts[cur_proc];
                ++idx;
            }
        }
    }
    // Sending initial unique portions of data to every process.
    MPI_Scatter(sendcounts, 1, MPI_INT, &tmp_count, 1, MPI_INT, 0, distr_comm);
    MPI_Scatterv(par->A_part, sendcounts, displs, mpi_sparse_elem_t,
                 tmp_elements, tmp_count, mpi_sparse_elem_t, 0, distr_comm);
    if (par->rank == 0) {
        recvcounts = sendcounts;
    }
    else {
        recvcounts = malloc(par->c * sizeof(*recvcounts));
        displs = malloc(par->c * sizeof(*displs));
    }

    // Now we can gather data among the replication groups.
    MPI_Allgather(&tmp_count, 1, MPI_INT, recvcounts, 1, MPI_INT, repl_comm);

    par->A_part_count = 0;
    next_displ = 0;
    for (i = 0; i < par->c; ++i) {
        par->A_part_count += recvcounts[i];
        displs[i] = next_displ;
        next_displ += recvcounts[i];
    }

    MPI_Allgatherv(tmp_elements, tmp_count, mpi_sparse_elem_t, par->A_part,
                   recvcounts, displs, mpi_sparse_elem_t, repl_comm);

    free(recvcounts);
    free(displs);
    free(tmp_elements);
    MPI_Comm_free(&distr_comm);
    MPI_Comm_free(&repl_comm);
}

static void do_single_multiplication(params_t *par) {
    int turn;
    int turns_num;
    int i;
    int z;
    int send_to = (par->j + par->pdivc - 1) % par->pdivc;
    int recv_from = (par->j + 1) % par->pdivc;
    double *C_ptr;
    double *B_ptr;
    MPI_Request send_req, recv_req;
    MPI_Status st;

    turns_num = (par->use_inner) ? par->q : par->pdivc;

    for (i = 0; i < par->BC_part_count; ++i)
        par->C_part[i] = 0;

    for (turn = 0; turn < turns_num; ++turn) {
        if (turn + 1 == turns_num && par->use_inner) {
            // it means we have to restore it to the state from the beginning
            send_to = (par->j + turn) % par->pdivc;
            recv_from = ((par->j - turn) % par->pdivc + par->pdivc)
                        % par->pdivc;
        }
        if (turns_num > 1) {
            MPI_Isend(par->A_part, par->A_part_count, mpi_sparse_elem_t, send_to,
                      MULTIPLY_TAG, par->shift_comm, &send_req);
            MPI_Irecv(par->A_part_tmp, par->nonz, mpi_sparse_elem_t, recv_from,
                      MULTIPLY_TAG, par->shift_comm, &recv_req);
        }

        for (i = 0; i < par->A_part_count; ++i) {
            B_ptr = par->B_part + (par->A_part[i].col * par->BC_col_width);
            C_ptr = par->C_part + (par->A_part[i].row * par->BC_col_width);
            for (z = 0; z < par->BC_col_width; ++z) {
                *C_ptr += par->A_part[i].elem * (*B_ptr);
                ++B_ptr;
                ++C_ptr;
            }
        }
        if (turns_num > 1) {
            MPI_Wait(&recv_req, &st);
            MPI_Get_count(&st, mpi_sparse_elem_t, &par->A_part_count);
            MPI_Wait(&send_req, &st);
            SWAP(sparse_elem_t*, par->A_part, par->A_part_tmp);
        }
    }
}

/* A function for preparing a communicator for the inner algorithm for
 * gathering the column parts of matrix C. We want to know the row offsets of
 * the parts that each process has so that we can just pass appropriate displs
 * to gather function to make it work.
 * Some care must be taken for the cases where some process contains a part
 * which is divided into two pieces. In such case we will copy the part which
 * overflows after the end of buffer (we allocated some additional space for
 * that) to make sending easier. Later every process will copy that back to
 * the beginning of buffer.
 */
static void inner_prepare_C_part(params_t *par, MPI_Comm *gather_comm,
                                 int **recvcounts, int **displs,
                                 int *who_overflows) {
    int l;

    MPI_Comm_split(MPI_COMM_WORLD, par->j, par->l, gather_comm);
    *recvcounts = malloc(par->c * sizeof(*recvcounts));
    *displs = malloc(par->c * sizeof(*displs));

    for (l = 0; l < par->c; ++l)
        // columns per one process piece = n / (p / c)
        // rows per one process piece = n / c
        (*recvcounts)[l] = par->n * par->n / par->p;

    for (l = 0; l < par->c; ++l) {
        int blk_nr = (l * par->q + par->j) % par->pdivc;
        // every process calculates q small blocks, whole gather group
        // calculates q*c small blocks = p/c small blocks, so each small block
        // contains n / (p/c) rows
        int blk_row_offset = blk_nr * par->n / par->pdivc;
        (*displs)[l] = blk_row_offset * par->BC_col_width;
        if (blk_row_offset + par->n / par->c > par->n)
            *who_overflows = l;
    }
}

static void inner_gather_C_part(params_t *par, MPI_Comm *gather_comm,
                                int *recvcounts, int *displs,
                                int *who_overflows) {
    int remaining = 0;

    if (*who_overflows != -1)
        remaining = recvcounts[*who_overflows] -
                    (par->BC_part_count - displs[*who_overflows]);

    if (par->l == *who_overflows)
        memcpy(par->C_part + par->BC_part_count, par->C_part,
               remaining * sizeof(*par->C_part));
    MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, par->C_part, recvcounts,
                   displs, MPI_DOUBLE, *gather_comm);
    if (*who_overflows != -1 && *who_overflows != par->l)
        memcpy(par->C_part, par->C_part + par->BC_part_count,
               remaining * sizeof(*par->C_part));
}

static void inner_cleanup_C_part(params_t *par, MPI_Comm *gather_comm,
                                 int *recvcounts, int *displs,
                                 int *who_overflows) {
    MPI_Comm_free(gather_comm);
    free(recvcounts);
    free(displs);
}

void calculate_C(params_t *par, int exponent) {
    MPI_Comm gather_comm = MPI_COMM_NULL;
    int i;
    int *recvcounts = NULL, *displs = NULL;
    int who_overflows = -1;
    int merging_needed = par->use_inner && par->c > 1;

    if (merging_needed)
        inner_prepare_C_part(par, &gather_comm, &recvcounts, &displs,
                             &who_overflows);

    for (i = 0; i < exponent; ++i) {
        do_single_multiplication(par);
        if (merging_needed)
            inner_gather_C_part(par, &gather_comm, recvcounts, displs,
                                &who_overflows);
        SWAP(double*, par->B_part, par->C_part);
    }
    SWAP(double*, par->B_part, par->C_part);

    if (merging_needed)
        inner_cleanup_C_part(par, &gather_comm, recvcounts, displs,
                             &who_overflows);
}

int count_elts_ge(params_t *par, double threshold) {
    int offset;
    int r, c;
    int count = 0;
    int gcount;

    offset = ((par->use_inner) ? par->j : par->rank) * par->BC_col_width;

    if (!par->use_inner || par->l == 0) {
        MPI_Comm reduce_comm;
        for (r = 0; r < par->n; ++r) {
            for (c = 0; c < par->BC_col_width; ++c) {
                int absc = c + offset;
                int maxrc = (r > absc) ? r : absc;
                if (maxrc < par->n_orig)
                    count += GET_C(par, r, c) >= threshold;
            }
        }
        reduce_comm = (par->use_inner) ? par->shift_comm : MPI_COMM_WORLD;
        MPI_Reduce(&count, &gcount, 1, MPI_INT, MPI_SUM, 0, reduce_comm);
    }

    return gcount;
}

void print_result(params_t *par) {
    double *C = NULL;
    int row, col;
    int buf_size = par->n * par->n;
    MPI_Comm gather_comm;

    if (par->rank == 0)
        C = malloc(buf_size * sizeof(double));

    gather_comm = (par->use_inner) ? par->shift_comm : MPI_COMM_WORLD;

    if (!par->use_inner || par->l == 0) {
        MPI_Gather(par->C_part, par->BC_part_count, MPI_DOUBLE, C,
                   par->BC_part_count, MPI_DOUBLE, 0, gather_comm);
    }

    if (par->rank == 0) {
        printf("%d %d\n", par->n_orig, par->n_orig);
        for (row = 0; row < par->n_orig; ++row) {
            for (col = 0; col < par->n_orig; ++col) {
                int proc_nr = col / par->BC_col_width;
                int offset = proc_nr * par->BC_part_count;
                int idx = offset + row * par->BC_col_width +
                          (col % par->BC_col_width);
                printf("%lf ", C[idx]);
            }
            printf("\n");
        }
        free(C);
    }
}
