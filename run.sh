#/bin/bash
L=$(for MAT in $(ls tests/matrix01*); do
    X=$(echo "$MAT" | cut -d '_' -f3)
    echo $X
done)
L=$(echo $L | tr ' ' '\n' | sort -nu)
arr=($L)
echo  ${arr[*]}

for TEST in $(ls tests/result*); do
    X=$(echo "$TEST" | cut -d '_' -f2)
    Y=$(echo "$TEST" | cut -d '_' -f3)
    Z=$(echo "$TEST" | cut -d '_' -f4)
    SEED=${arr[10#$Z]}
    if [ $Z -eq 3 ]
    then
        SEED=${arr[4]}
    else
        if [ $Z -eq 4 ]
        then
            SEED=${arr[3]}
        else
            if [ $Z -eq 5 ]
            then
                SEED=${arr[8]}
            else
                if [ $Z -eq 6 ]
                then
                    SEED=${arr[9]}
                else
                    if [ $Z -eq 8 ]
                    then
                        SEED=${arr[5]}
                    else
                        if [ $Z -eq 9 ]
                        then
                            SEED=${arr[6]}
                        fi
                    fi
                fi
            fi
        fi
    fi

    mpirun -np 6 ./matrixmul -f tests/sparse05_${Y}_${Z} -s $SEED -c 2 -v -e $X > temp_res
    ./compare temp_res "tests/result_${X}_${Y}_${Z}_${SEED}"
    if [ $? -eq 0  ]
    then
        echo "TEST tests/result_${X}_${Y}_${Z} PASSED"
    else
        echo -e "\e[31m"
        echo "TEST tests/result_${X}_${Y}_${Z} NOT PASSED"
        echo "RUN FOLLOWING COMMAND"
        echo "mpirun -np 6 ./matrixmul -f tests/sparse05_${Y}_${Z} -s $SEED -c 2 -v -e $X > temp_res"
        echo "./compare temp_res tests/result_${X}_${Y}_${Z}"
        echo -e "\e[39m"
        exit
    fi
    mpirun -np 8 ./matrixmul -f tests/sparse05_${Y}_${Z} -s $SEED -c 2 -v -e $X -i > temp_res
    ./compare temp_res "tests/result_${X}_${Y}_${Z}_${SEED}"
    if [ $? -eq 0  ]
    then
        echo "TEST INNER tests/result_${X}_${Y}_${Z} PASSED"
    else
        echo -e "\e[31m"
        echo "TEST INNER tests/result_${X}_${Y}_${Z} NOT PASSED"
        echo "RUN FOLLOWING COMMAND"
        echo "mpirun -np 8 ./matrixmul -f tests/sparse05_${Y}_${Z} -s $SEED -c 2 -v -e $X > temp_res -i"
        echo "./compare temp_res tests/result_${X}_${Y}_${Z}"
        echo -e "\e[39m"
        exit
    fi
    rm -f temp_res
done
